import { MachineService } from './machine.service';
import { Controller, Get } from '@nestjs/common';


@Controller('machine')
export class MachineController {
  constructor(private readonly machineService: MachineService) {}

  @Get()
  getMachine(): string {
    return this.machineService.getMachine();
  }
}
