import { Injectable } from '@nestjs/common';

@Injectable()
export class MachineService {
  getMachine(): string {
    return 'I\'m a machine!';
  }
}
