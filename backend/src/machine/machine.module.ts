import { MachineService } from './machine.service';
import { MachineController } from './machine.controller';
import { Module } from '@nestjs/common';

@Module({
  imports: [],
  controllers: [MachineController],
  providers: [MachineService],
})
export class MachineModule {}
